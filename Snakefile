import os

# user specified
reads = config["reads"]
gblocks = config["gblocks"]
inserts = config["inserts"]

# defaults
backbone = config.get("backbone", workflow.basedir + "/data/LOOP_backbone_components.fna")
marker = config.get("marker", workflow.basedir + "/data/Selectable_markers.fna")

# GLOBALS
BLAST_FMT = '"6 sseqid qseqid sstrand slen sstart send qlen qstart qend length evalue pident"'



example_spell = """
snakemake -j 2 -p -d ../Barcode1(3blocks)Results --use-conda --config \
    reads=../ConcatReads/RXN1/RXN1_BarcodeReads.fastq \
    gblocks=../gblocks_cdEDITS.fna \
"""


rule all:
    input:
        "plasmid_types.counts.tsv",
        "loopseq_viz.html"


rule fastq_to_fasta:
    input:
        reads
    output:
        "seqrecords.fna"
    conda:
        "requirements.yml"
    script:
        "scripts/fastq_to_fasta.py"


rule cp_defaults:
    input:
        backbone,
        marker
    output:
        os.path.basename(backbone),
        os.path.basename(marker)
    shell:
        """cp {input} .
        """


rule cp_gblocks:
    input:
        gblocks
    output:
        "gblocks.fna"
    shell:
        """ cp {input} {output}
        """


rule blast:
    input:
        '{some}.fna',
        rules.fastq_to_fasta.output[0]
    output:
        '{some}.seqrecords.blast.tbl'
    conda:
        "requirements.yml"
    params:
        outfmt = BLAST_FMT
    shell:
        "blastn -subject {input[1]} -query {input[0]} -outfmt {params[outfmt]} > {output}"


rule parse_blast_results:
    input:
        'Selectable_markers.seqrecords.blast.tbl',
        'LOOP_backbone_components.seqrecords.blast.tbl',
        'gblocks.seqrecords.blast.tbl',
        reads
    output:
        "plasmid_types.counts.tsv",
        'full_length_reads.fasta',
        'SummaryStats.tsv',
        "read_types.tsv"
    params:
        insert_count = inserts
    conda:
        "requirements.yml"
    script:
        "scripts/loopseq_quantification.py"


rule visualize_results:
    input:
        rules.parse_blast_results.output[0],
    output:
        "loopseq_viz.html"
    conda:
        "requirements.yml"
    script:
        "scripts/loopseq_viz.py"


rule visualize_full_length_reads:
    input:
        rules.parse_blast_results.output[0]
    output:
        "full_length_reads.heatmap.html"
    conda:
        "requirements.yml"
    script:
        "scripts/full_length_heatmap.py"


rule visualize_plasmid_percentages:
    input:
        rules.parse_blast_results.output[0]
    output:
        "plasmid_percent_piechart.html"
    conda:
        "requirements.yml"
    script:
        "scripts/plasmid_piechart.py"
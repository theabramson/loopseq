from Bio import SeqIO

gblocks_path = snakemake.input[0]
shorts_path = snakemake.output[0]
long_path = snakemake.output[1]


#parses the file consisting of loop parts (in this case gblocks) and seperates them 
#into 2 different files based on length (30pb threshold)
longblocks = []
shortblocks = []
for record in SeqIO.parse(gblocks_path,"fasta"):
    if len(record.seq) > 30:
        longblocks.append(record)
    else:
        shortblocks.append(record)
          
SeqIO.write(longblocks, long_path, "fasta")        
SeqIO.write(shortblocks, shorts_path,"fasta")

from Bio import SeqIO

# snakemake script to convert fastq to fasta
fastq_input = snakemake.input[0]
fasta_output = snakemake.output[0]
SeqIO.convert(fastq_input, "fastq", fasta_output, "fasta")
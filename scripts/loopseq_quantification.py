#dependencies 
import pandas as pd
from Bio import SeqIO
import os

backbone_key = "pMB1_ori"


def load_blast(queryfile):
    blast_output = pd.read_csv(queryfile, sep = '\t')
    blast_output.columns = [
        "ReadID", "gBlock", "SubjectStrand", "SubjectLength", "SubjectStart",
        "SubjectEnd", "QueryLength", "QueryStart", "QueryEnd", "AlignmentLength",
        "evalue", "PercentIdentity"
    ]
    blast_output["real_start"] = blast_output[["SubjectStart", "SubjectEnd"]].min(axis=1)
    blast_output["real_end"] = blast_output[["SubjectStart", "SubjectEnd"]].max(axis=1)
    blast_output["coverage"] = blast_output.AlignmentLength / blast_output.QueryLength
    return blast_output 


def filter_overlaps(df):
    """ Given a dataframe containing all hits for a read, check for overlaps
        between the hits and drop the hits with the lower pident

        TODO: Consider better criteria than merely higher pident
    """
    df = df.sort_values("real_start")
    cur_row = df.iloc[0]
    cur_index = df.index[0]
    rows_to_drop = []
    for i, row in df.iloc[1:].iterrows():
        if row.real_start - cur_row.real_end < -49:
            if(cur_row.PercentIdentity > row.PercentIdentity):
                rows_to_drop.append(i)
            else:
                rows_to_drop.append(cur_index)
                cur_row = row
                cur_index = i
        else:
            cur_row = row
            cur_index = i 
    return df.drop(rows_to_drop)        


bblist = {
    "p0dd_backbone", "pCAe_bb", "pCAo_bb", "pEven_bb",
    "pGreenII_bb", "pICE_Low_copy_bb", "pL0R-lacZ-4_bb"
}


def filter_backbone(blastbackbone, bbelements=tuple(bblist)):
    """ given a dataframe containing hits for a read, 
        check for instances for every backbone component
        and return true if all elements are found
    """
    bbset = set(bbelements)
    return any(m in bbset for m in bbelements)


def filter_markers(blastmarkers):
    """ TODO: write doctstring
    """
    return len(blastmarkers) > 0


def make_table(read_set,gblockDF,backboneDF,markerDF):
    newDF = pd.DataFrame()
    backboneDF = backboneDF[backboneDF.gBlock == backbone_key]
    for curr_read in read_set:
        select_blockrows = gblockDF.loc[gblockDF['ReadID'] == curr_read]
        select_backbonerows = backboneDF.loc[backboneDF['ReadID'] == curr_read]
        newDF = newDF.append([select_blockrows, select_backbonerows])
    newDF = newDF.loc[:, ['ReadID','SubjectStrand','gBlock','real_start']]
    newDF = newDF.sort_values(["ReadID", "real_start"])
    return   newDF


def fix_strand(df): #output from make_table
    df = df.copy()
    ori = df[df["gBlock"]== backbone_key]
    if(len(ori) != 1):
        print(f"this is a bad read: {df.iloc[0, 0]}")
        #if len(ori) < 1:
        return pd.DataFrame([], columns=df.columns)
    if ori.iloc[0].SubjectStrand == "minus":
        #go back modify new df using input df
        df['SubjectStrand'] = df["SubjectStrand"].apply(lambda x: "minus" if(x == "plus") else "plus")
        df = df.iloc[::-1]
    return df


def make_tuples(df):
    #(readID(gblock(start),gblock(start),gblock(start))readID,(gblock(start),gblock(start)))
    """iterate through a groupby object??"""
    ret = list(zip(df.gBlock, df.SubjectStrand))
    ori_pos = ret.index((backbone_key, "plus"))
    ret = ret[ori_pos:] + ret[:ori_pos]
    return tuple(ret)

def make_file(fileName,IDList, outpath):
    outsequences = open(outpath, "w")
    count = 0 
    fulllength = 0
    totallength = 0 
    for record in SeqIO.parse(fileName,"fastq"):
        count += 1
        totallength += len(record.seq)
        for ID in IDList:
            if ID == record.id:
                fulllength += len(record.seq)
                outsequences.write(">" + ID + "\n" + str(record.seq) + "\n")
    outsequences.close()
    return count, fulllength, totallength

def main():
    # get blast paths
    marker_path = snakemake.input[0]
    backbone_path = snakemake.input[1]
    blocks_path = snakemake.input[2]
    reads_path = snakemake.input[3]
    output_path = snakemake.output[0]
    valid_reads_path = snakemake.output[1]
    summarystats_path = snakemake.output[2]
    read_types_path = snakemake.output[3]
    insert_number = snakemake.params["insert_count"]

    # load gblocks and add fields
    blastblocks = load_blast(blocks_path)
    #blastblocks = pd.concat([longblocks, shortblocks])
    # filter alignments to non-overlapping, long, and higher pident
    blastblocks = blastblocks[blastblocks.coverage > 0.7]
    blastblocks = blastblocks.groupby("ReadID").apply(filter_overlaps).reset_index(drop=True)
    if(len(blastblocks) == 0):
        raise ValueError("No valid gblock alignments found after filtering")
    # identify reads with at least 'insert_number' valid gblocks
    gblock_counts = blastblocks.groupby("ReadID").apply(len)
    valid_reads_gblocks = set(gblock_counts[gblock_counts == insert_number].index)

    # handle backbone
    blastbackbone = load_blast(backbone_path)
    backbone_tests = blastbackbone.groupby("ReadID").apply(filter_backbone)
    valid_reads_backbone = set(backbone_tests[backbone_tests].index)

    # handle selectable markers
    blastmarkers = load_blast(marker_path)
    # marker_tests = blastmarkers.groupby("ReadID").apply(filter_markers)
    # valid_reads_markers = set(marker_tests[marker_tests].index)
    valid_reads_markers = set(blastmarkers.ReadID)

    # Identify complete reads and quantify
    valid_reads = valid_reads_markers & valid_reads_gblocks & valid_reads_backbone
    valid_DF = make_table(valid_reads,blastblocks,blastbackbone,blastmarkers)
    valid_DF = valid_DF.groupby("ReadID").apply(fix_strand).reset_index(drop=True)
    read_types = valid_DF.groupby("ReadID").apply(make_tuples)
    read_types.to_csv(read_types_path, sep="\t")
    read_types.value_counts().to_csv(output_path, sep="\t")

    totalreads, fullnuclength, totalnuclength = make_file(reads_path, valid_reads, valid_reads_path)

    stats = {
        "FullLengthReads":len(read_types),
        "TotalReads":totalreads,
        "UniqeCount":len(read_types.value_counts()),
        "FullReadsNucLength": fullnuclength,
        "TotalReadsNuclength": totalnuclength,
        "NucLengthPercent" : fullnuclength/totalnuclength*100,
        "FullAvgLength": fullnuclength/len(read_types),
        "TotalAvgLength": totalnuclength/totalreads
    }
    stats["FullLengthPercent"] = stats["FullLengthReads"]/stats["TotalReads"]*100

    pd.Series(stats).to_frame().transpose().to_csv(summarystats_path, sep = "\t", index = None)


if(__name__ == "__main__"):
    main()


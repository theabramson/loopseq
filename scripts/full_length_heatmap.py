import pandas
from bokeh.plotting import figure, output_file, save
from bokeh.transform import dodge, factor_cmap
from bokeh.palettes import Spectral6
from bokeh.models import ColumnDataSource, Legend
from bokeh.layouts import column
from bokeh.models import HoverTool, Title

from numpy.random import randint


def load_read_types(fpath):
    ret = pandas.read_csv(fpath, sep="\t")
    ret.columns = ["components", "freq"]
    ret.components = ret.components.apply(eval).apply(list).str[1:]
    comp_lens = ret.components.apply(len)
    if(any(comp_lens[0] != l for l in comp_lens)):
        raise ValueError("unequal length components detected")
    print(ret)
    return ret



def draw_figure(counts, outpath):
    """
    """
    # Load data and convert to read X num(positions) dataframe
    output_file(outpath)
    comp_positions = len(counts.components.iloc[0])
    reads_df = pandas.DataFrame(
        {
            f"block_{i}": [
                blk
                for blk, c in zip(counts.components.str[i], counts.freq)
                for j in range(c)
            ]
            for i in range(comp_positions)
        }
    )

    # define color maps to be used
    colors = [
        "#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78",
        "#2ca02c", "#98df8a", "#d62728", "#ff9896",
        "#9467bd", "#c5b0d5", "#8c564b", "#c49c94",
        "#e377c2", "#f7b6d2", "#7f7f7f", "#c7c7c7",
        "#bcbd22", "#dbdb8d", "#17becf", "#9edae5"
    ]
    color_map = {
        pos: {
            gblock: colors[i]
            for i, gblock in enumerate(reads_df[pos].unique())
        }
        for pos in reads_df.columns
    }
    
    # convert from flat to tidy frame
    tidy_df = reads_df.stack().to_frame().reset_index()
    tidy_df.columns = ["read", "pos", "gblock"]
    tidy_df["color"] = [color_map[p][g] for p, g in zip(tidy_df.pos, tidy_df.gblock)]
    tidy_df.to_csv("temp.tsv", sep="\t")
    tidy_df = tidy_df.applymap(str)
    cds = ColumnDataSource(tidy_df)

    legends = []
    for pos, mapping in color_map.items():
        f = figure(
            plot_width = 300 * len(reads_df.columns),
            plot_height=50,
            title=f"{pos} legend",
            x_range = [" ".join(i) for i in mapping],
            y_range = ["0"]
        )
        temp = pandas.DataFrame({
            "dummy_pos": "0",
            "gblock": [" ".join(i) for i in mapping.keys()],
            "color": list(mapping.values())
        })
        print(temp)
        lsource = ColumnDataSource(temp)
        f.rect("gblock", "dummy_pos", 1, 0.95, source=lsource, fill_alpha=0.6, color="color")
        f.outline_line_color = None
        f.grid.grid_line_color = None
        f.axis.axis_line_color = None
        f.axis.major_tick_line_color = None
        f.axis.major_label_standoff = 0
        f.yaxis.visible = False
        legends.append(f)
    

    # Draw heatmap TODO: Add num(positions) legends to figure and potentionally tooltips
    hover1 = HoverTool(tooltips=[("gBlock", "@gblock"),("read","@read")])
    fig = figure(
        tools = [hover1],
        plot_width=300 * len(reads_df.columns),
        plot_height=500,  # 10*len(reads_df),
        title="Full Length Reads",
        x_range=list(reads_df.columns),
        y_range=[str(i) for i in reversed(range(len(reads_df)))],
    )

    fig.rect("pos", "read", 1, 0.95, source=cds, fill_alpha=0.6, color='color')
    
    
    fig.outline_line_color = None
    fig.grid.grid_line_color = None
    fig.axis.axis_line_color = None
    fig.axis.major_tick_line_color = None
    fig.axis.major_label_standoff = 0
    fig.yaxis.visible = False
    fig.add_layout(Title(text="Description: Each row represents a single full length plasmid. Each column represents a gBLock insertion position.", align="left",text_font_size='8pt'), "below")
    legends.append(fig)

    save(column(*legends))


def main():
    read_counts = snakemake.input[0]
    outpath = snakemake.output[0]
    counts = load_read_types(read_counts)
    draw_figure(counts, outpath)

if(__name__ == "__main__"):
    main()
